const { Text } = require('../src/js/Text')

describe('Text', () => {
  it('to string', () => {
    expect(Text.toString()).toEqual('someText')
  })
})