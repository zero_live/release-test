FROM node:latest

WORKDIR /opt/release
COPY package* ./

RUN npm install

COPY . .